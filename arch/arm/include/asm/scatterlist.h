#ifndef _ASMARM_SCATTERLIST_H
#define _ASMARM_SCATTERLIST_H

#include <asm/memory.h>
#include <asm/types.h>

#include <asm-generic/scatterlist.h>

#undef ARCH_HAS_SG_CHAIN

#endif /* _ASMARM_SCATTERLIST_H */
